import requests
import os

ETHERSCAN_API_KEY =  os.getenv("ETHERSCAN_API_KEY")
BINANCE_API_URL = os.getenv("BINANCE_API_URL", "https://api.binance.com/api/v3/ticker/price?symbol=ETHUSDT")

def fetch_transactions(etherscan_api_key, address, start_block=0, end_block='latest', page=1, offset=100):
    url = f"https://api.etherscan.io/api?module=account&action=tokentx&address={address}&startblock={start_block}&endblock={end_block}&page={page}&offset={offset}&sort=asc&apikey={etherscan_api_key}"
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()['result']
    return []

def fetch_eth_usdt_price():
    url = "https://api.binance.com/api/v3/ticker/price?symbol=ETHUSDT"
    response = requests.get(url)
    if response.status_code == 200:
        return float(response.json()['price'])
    return 0.0