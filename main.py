from fastapi import FastAPI, Depends, HTTPException, BackgroundTasks
from sqlalchemy.orm import Session
from model import Base, TransactionFee, engine, SessionLocal
from external_apis import fetch_transaction_fee, fetch_eth_usdt_price

app = FastAPI()

# Create database tables
Base.metadata.create_all(bind=engine)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

def process_transaction_fee(tx_hash: str, eth_fee: float, binance_api_url: str):
    usdt_price = fetch_eth_usdt_price(binance_api_url)
    fee_in_usdt = eth_fee * usdt_price

    db = SessionLocal()
    db_transaction_fee = TransactionFee(tx_hash=tx_hash, fee_in_usdt=fee_in_usdt)
    db.add(db_transaction_fee)
    db.commit()
    db.close()

@app.post("/record_transaction/")
async def record_transaction(tx_hash: str, db: Session = Depends(get_db)):
    eth_fee = fetch_transaction_fee(tx_hash)
    usdt_price = fetch_eth_usdt_price()
    fee_in_usdt = eth_fee * usdt_price

    db_transaction_fee = TransactionFee(tx_hash=tx_hash, fee_in_usdt=fee_in_usdt)
    db.add(db_transaction_fee)
    db.commit()
    db.refresh(db_transaction_fee)
    return db_transaction_fee

@app.get("/transaction_fee/{tx_hash}")
async def get_transaction_fee(tx_hash: str, db: Session = Depends(get_db)):
    transaction_fee = db.query(TransactionFee).filter(TransactionFee.tx_hash == tx_hash).first()
    if transaction_fee is None:
        raise HTTPException(status_code=404, detail="Transaction not found")
    return transaction_fee


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.on_event("startup")
async def startup_event(background_tasks: BackgroundTasks)