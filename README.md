
#### Virtual Envrironment
* python -m venv venv
* source venv/bin/activate (linux) or .\venv\Scripts\activate (windows)


#### Install required dependencies
* pip install -r requirements.txt


#### Run App (TODO)
* uvicorn main:app --reload

#### Swagger UI
* http://127.0.0.1:8000/docs

#### Design considerations (TODO)

- Backend:
* FastAPI for building RESTful API due to its performance and ease of use. + include swagger documentation. Fast API also supports many useful features like Background tasks, events, etc.
* Implement async requests to external APIs for non-blocking IO operations.
- Database:
* PostgreSQL is considered for its robustness, scalability, and support for complex queries
* SQLAlchemy for ORM to interface in python
- Caching:
  