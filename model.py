from sqlalchemy import Column, Integer, String, Float, DateTime
from database import Base

class TransactionFee(Base):
    __tablename__ = "transaction_fees"
    tx_hash = Column(String, primary_key=True)
    fee_in_usdt = Column(Float)
    fee_in_eth = Column(Float)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
